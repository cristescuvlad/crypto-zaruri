#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <networkconnection.h>
#include <gamewindow.h>
#include <networkdata.h>

#include <rsa.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void authenticate();
	void recreateGameWindow();

    void sendAuthData();
    void sendKeyToPeer();
    void sendNetworkDataToPeer();
    bool receiveKeyFromPeer();
    bool receiveNetworkDataFromPeer();

	bool setCurrentSessionKeys(const QString& keysAsString);
    void updateNwData();

private slots:
    void onAuthenticated();
	void onConnectToNetworkButtonClicked();
	void onStartServerButtonClicked();
	void onGenRSAButtonClicked();
	void onGameWindowClosed();
    void receiveAuthData();

private:
    Ui::MainWindow *ui;

	NetworkConnection* m_peerConnection = nullptr;
	GameWindow* m_gameWindow = nullptr;
	QTimer* m_timer = nullptr;

    NetworkData m_networkData;
	public_key_class m_pubKey;
	private_key_class m_privKey;
	public_key_class m_peerPubKey;

	QHostAddress m_peerAddress;
    bool m_isHost = false;
    bool m_receivedKey = false;
    bool m_receivedNetworkData = false;

};

#endif // MAINWINDOW_H
