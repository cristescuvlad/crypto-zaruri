#include "mainwindow.h"
#include <QApplication>
#include <QDateTime>
#include <rsa.h>

int main(int argc, char *argv[])
{
    //TODO: instances open the same time
    qsrand(QDateTime::currentMSecsSinceEpoch());
    rsa_init();

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
