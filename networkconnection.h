#ifndef NETWORKCONNECTION_H
#define NETWORKCONNECTION_H

#include <QObject>
#include <QTcpSocket>
#include <QTcpServer>

#define PORT 7621
#define TIMEOUT 5000

class NetworkConnection : public QObject
{
    Q_OBJECT
public:
    explicit NetworkConnection(QObject *parent = nullptr);
	~NetworkConnection();
	bool connectToHost(QHostAddress address);
	void disconnectFromHost();
	bool listenToHost(QHostAddress address, quint16 port);
	int sendData(const QByteArray* data);
	QByteArray* getReadData() const; //any data that comes to the socket is stored in m_buffer 

private:
	void handleSocketEvents();

signals:
	void readyRead(); //emmited when onReadyRead() is triggered by m_socket->readyRead()
	void connectionStopped();

private slots:
	void onReadyRead();
	void onSocketDisconnected();
	
private:
	QTcpSocket* m_socket = nullptr;
    QByteArray* m_buffer = nullptr;
};

#endif // NETWORKCONNECTION_H