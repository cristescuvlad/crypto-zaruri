#-------------------------------------------------
#
# Project created by QtCreator 2017-10-14T10:01:47
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Crypto_Zaruri
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    networkconnection.cpp \
    gamewindow.cpp \
    rsa.cpp \
    networkdata.cpp \
    Merkle/mt_arr_list.c \
    Merkle/mt_crypto.c \
    Merkle/mt_impl.c \
    Merkle/sha224-256.c

HEADERS += \
        mainwindow.h \
    networkconnection.h \
    gamewindow.h \
    rsa.h \
    networkdata.h \
    utils.h \
    Merkle/merkletree.h \
    Merkle/mt_arr_list.h \
    Merkle/mt_config.h \
    Merkle/mt_crypto.h \
    Merkle/mt_err.h \
    Merkle/sha.h \

FORMS += \
        mainwindow.ui \
    gamewindow.ui
