#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "utils.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDataStream>

#include <QFile>
#include <QTimer>
#include <QDateTime>

#define NETWORK_DATA_FILE "networkData.txt"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this); 
	
	m_peerConnection = new NetworkConnection();
    m_timer = new QTimer(this);

	connect(ui->ConnectToNetworkButton, SIGNAL(clicked()), this, SLOT(onConnectToNetworkButtonClicked()));
    connect(ui->StartServerButton, SIGNAL(clicked()), this, SLOT(onStartServerButtonClicked()));
	connect(ui->GenRSAPushButton, SIGNAL(clicked()), this, SLOT(onGenRSAButtonClicked()));
	
	m_networkData.init(NETWORK_DATA_FILE);
}

MainWindow::~MainWindow()
{
	delete ui;
	delete m_peerConnection;
    delete m_timer;
}

void MainWindow::authenticate() {

    connect(m_timer, SIGNAL(timeout()), this, SLOT(receiveAuthData()));
    m_timer->start();

    sendAuthData();
}

void MainWindow::recreateGameWindow() {
	
	//m_gameWindow has delete on close attribute. there should be no mem leak
	m_gameWindow = new GameWindow(m_peerConnection, m_isHost, m_networkData.getBalance(m_pubKey));
	connect(m_gameWindow, SIGNAL(windowClosed()), SLOT(onGameWindowClosed()));
	m_gameWindow->setVisible(true);
}

void MainWindow::sendAuthData() {
	sendKeyToPeer(); //TODO: error check
    sendNetworkDataToPeer();
}

void MainWindow::sendKeyToPeer() {

    QJsonObject jsonObj;
    jsonObj.insert("expo", QJsonValue::fromVariant(m_pubKey.exponent));
    jsonObj.insert("mod", QJsonValue::fromVariant(m_pubKey.modulus));

    QJsonDocument jsonDoc(jsonObj);
    QByteArray jsonBytes = jsonDoc.toJson(QJsonDocument::Compact);
    int jsonSize = jsonBytes.size();

    QByteArray dataToSend;
    dataToSend.append((char*)&jsonSize,sizeof(int));
    dataToSend.append(jsonBytes); 

    if (m_peerConnection->sendData(&dataToSend) != dataToSend.size()) {
		fprintf(stdout, "Warning: Sent wrong amount of data.\n");
    }
}

void MainWindow::sendNetworkDataToPeer() {

	QJsonObject jsonObj;

	for (auto key : m_networkData.getDataKeys()) {
		jsonObj.insert(key, m_networkData.getValue(key));
	}

	QJsonDocument jsonDoc(jsonObj);
	QByteArray jsonBytes = jsonDoc.toJson(QJsonDocument::Compact);
	int jsonSize = jsonBytes.size();

	QByteArray dataToSend;
	dataToSend.append((char*)&jsonSize, sizeof(int));
	dataToSend.append(jsonBytes);

	if (m_peerConnection->sendData(&dataToSend) != dataToSend.size()) {
		fprintf(stdout, "Warning: Sent wrong amount of data.\n");
	}
}

bool MainWindow::receiveKeyFromPeer() {
        
    QByteArray* receivedData = m_peerConnection->getReadData();
    
    if (receivedData->isEmpty()) {
        //TODO: Log failed to receive data
        return false;
    }

    if (receivedData->size() < sizeof(int)) {
        return false;
    }

    QDataStream stream(*receivedData);
    stream.setByteOrder(QDataStream::LittleEndian);

    int jsonSize = 0;
    stream.readRawData((char*)&jsonSize, sizeof(int));

    if (jsonSize == 0) {
        return false;
    }

    if (receivedData->mid(sizeof(int)).size() < jsonSize) { //starting after the 4 bytes until the end
        return false;
    }

    QJsonObject receivedJsonObj = QJsonDocument::fromJson(receivedData->mid(sizeof(int), jsonSize)).object();

	receivedData->remove(0, sizeof(int) + jsonSize);

    m_peerPubKey.exponent = receivedJsonObj["expo"].toVariant().toLongLong();
    m_peerPubKey.modulus = receivedJsonObj["mod"].toVariant().toLongLong();

    return true;
}

bool MainWindow::receiveNetworkDataFromPeer() {

    QByteArray* receivedData = m_peerConnection->getReadData();

	if (receivedData->isEmpty()) {
		//TODO: Log failed to receive data
		return false;
	}

	if (receivedData->size() < sizeof(int)) {
		return false;
	}

	QDataStream stream(*receivedData);
	stream.setByteOrder(QDataStream::LittleEndian);

	int jsonSize = 0;
	stream.readRawData((char*)&jsonSize, sizeof(int));

	if (jsonSize == 0) {
		return false;
	}

	if (receivedData->mid(sizeof(int)).size() < jsonSize) { //starting after the 4 bytes until the end
		return false;
	}

	QJsonObject jsonObj = QJsonDocument::fromJson(receivedData->mid(sizeof(int), jsonSize)).object();

	receivedData->remove(0, sizeof(int) + jsonSize);

	for (auto key : jsonObj.keys()) {
		m_networkData.addData(key, jsonObj.value(key).toString());
	}

    return true;
}

bool MainWindow::setCurrentSessionKeys(const QString& keysAsString) {

	if (fromFormatStr(keysAsString).size() < 4) {
		return false;
	}

	FmtStrToKeys(keysAsString, m_pubKey, m_privKey);

	return true;
}

void MainWindow::updateNwData() {

    qlonglong userOldBalance = m_networkData.getValue(keyToFmtStr(m_pubKey)).toLongLong();
    qlonglong peerOldBalance = m_networkData.getValue(keyToFmtStr(m_peerPubKey)).toLongLong();
    qlonglong peerNewBalance = peerOldBalance + m_gameWindow->getLocalUserBalanceFromGame() - userOldBalance;
    qint64 currentTime = QDateTime::currentMSecsSinceEpoch();

    QString updatedLocalData = formatStrs({
        QString::number(m_gameWindow->getLocalUserBalanceFromGame()),
        QString::number(currentTime)
    });
    m_networkData.addData(keyToFmtStr(m_pubKey), updatedLocalData);

    QString updatedPeerData = formatStrs({
        QString::number(peerNewBalance),
        QString::number(currentTime)
    });
    m_networkData.addData(keyToFmtStr(m_pubKey), updatedLocalData);
}

void MainWindow::onAuthenticated() {
    m_timer->stop();
	disconnect(m_timer, SIGNAL(timeout()), this, SLOT(receiveAuthData()));
    this->setVisible(false);
    recreateGameWindow();
}

void MainWindow::onConnectToNetworkButtonClicked() {

	QString insertedKeys = ui->InsertKeysTextEdit->toPlainText();

	if (insertedKeys.isEmpty()) {
        ui->ConnectingLabel->setText("Please enter the authentication keys\nformat: pubE:pubM:privE:privM)");
        return;
    }

	if (!setCurrentSessionKeys(insertedKeys)) {
		ui->ConnectingLabel->setText("Invalid keys! Please enter the authentication keys\nformat: pubE:pubM:privE:privM)");
	}

	QString value = formatStrs({
		QString::number(m_networkData.getBalance(m_pubKey)),
		QString::number(QDateTime::currentMSecsSinceEpoch())
	});

	m_networkData.addData(keyToFmtStr(m_pubKey), value);

	if (m_peerAddress.setAddress(ui->PeerIPLineEdit->text())) {
		
		if (m_peerConnection->connectToHost(m_peerAddress)) {
			ui->ConnectingLabel->setText("Connection to " + m_peerAddress.toString() + " succeded!");

            m_isHost = false;
			authenticate();
		}
		else {
            m_peerConnection->disconnectFromHost();
			ui->ConnectingLabel->setText("Connection to " + m_peerAddress.toString() + " failed");
		}
	}
	else {
		ui->ConnectingLabel->setText("Invalid IP address");
	}
}

void MainWindow::onStartServerButtonClicked() {

	QString insertedKeys = ui->InsertKeysTextEdit->toPlainText();

	if (insertedKeys.isEmpty()) {
		ui->ServerLabel->setText("Please enter the authentication keys\nformat: pubE:pubM:privE:privM");
		return;
	}

	if (!setCurrentSessionKeys(insertedKeys)) {
		ui->ServerLabel->setText("Invalid keys! Please enter the authentication keys\nformat: pubE:pubM:privE:privM)");
	}

	QString value = formatStrs({ 
		QString::number(m_networkData.getBalance(m_pubKey)),
		QString::number(QDateTime::currentMSecsSinceEpoch())
	});

	m_networkData.addData(keyToFmtStr(m_pubKey), value);

	ui->ServerLabel->setText("Waiting for a connection");

	if (m_peerConnection->listenToHost(QHostAddress::Any, PORT)) {

		ui->ServerLabel->setText("Server received a connection");

        m_isHost = true;
        authenticate();
	}
	else {
        m_peerConnection->disconnectFromHost();
		ui->ServerLabel->setText("Server did not receive any connections");
	}
}

void MainWindow::onGenRSAButtonClicked() {

	//TODO: replace with propper rsa
    if (!rsa_gen_keys(&m_pubKey, &m_privKey, PRIME_SRC_FILE)) {
        ui->InsertKeysTextEdit->setText("Could not generate keys. Maybe missing primes file?");
        return;
    }

	QString concatKeys = formatStrs({
		keyToFmtStr(m_pubKey),
		keyToFmtStr(m_privKey) 
	});

	ui->InsertKeysTextEdit->setText(concatKeys);
}

void MainWindow::onGameWindowClosed() {
	
    m_peerConnection->disconnectFromHost();

    updateNwData();
    m_networkData.init(NETWORK_DATA_FILE); //reinit with latest data

	m_isHost = false;
	m_receivedKey = false;
	m_receivedNetworkData = false;
	ui->ServerLabel->setText("");
	ui->ConnectingLabel->setText("");
	this->setVisible(true);
	m_gameWindow->close(); //has delete on close attribute
}

void MainWindow::receiveAuthData() {
    if (!m_receivedKey){
        if (receiveKeyFromPeer()) {
			m_receivedKey = true;
        }
		return;
    }
    else {
        if (!m_receivedNetworkData) {
            if (!receiveNetworkDataFromPeer()) {
				m_receivedNetworkData = true;
            }
        }
		//return; //do not return so that onAuthenticated gets called
    }
    onAuthenticated(); //TODO: move somewhere else
}
