#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <QWidget>

#include "networkconnection.h"

namespace Ui {
class GameWindow;
}

class GameWindow : public QWidget
{
    Q_OBJECT

public:
    explicit GameWindow(NetworkConnection* peerConnection, bool isHost, const qlonglong& currentBalance, QWidget *parent = 0);
    ~GameWindow();
	qlonglong getLocalUserBalanceFromGame();

private:
	void sendGameDataToNetwork();
	bool isMyTurn() const;
	void closeEvent(QCloseEvent *event) override;
	void updateGameValues();

signals:
	void windowClosed();

private slots:
	void onRollADieButtonClicked();
	void onConnectionStopped();
    void receiveGameDataFromNetwork();
	void updateWindow();

private:
    Ui::GameWindow *ui;
	NetworkConnection* m_peerConnection = nullptr;
    QTimer* m_timer = nullptr;

	bool m_isHost = false;

	qlonglong m_currentBalance = 0;
	int  m_rollValue = -1;
	qlonglong m_betValue = -1;
	int m_opponentRollValue = -1;
	qlonglong m_opponentBetValue = -1;
};

#endif // GAMEWINDOW_H
