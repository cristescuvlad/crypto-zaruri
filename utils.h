#ifndef UTILS_H
#define UTILS_H

#include <QString>
#include <QStringList>
#include <rsa.h>

static QString formatStrs(std::initializer_list<QString> strings){
	QString ret;
	for (auto it : strings) {
		ret.append(it + ":");
	}
	ret.chop(1);
	return ret;
}

static QStringList fromFormatStr(const QString& fmt_str) {
	return fmt_str.split(":");
}

static QString keyToFmtStr(const public_key_class& userKey) {
	return formatStrs({ QString::number(userKey.exponent), QString::number(userKey.modulus) });
}

static QString keyToFmtStr(const private_key_class& userKey) {
	return formatStrs({ QString::number(userKey.exponent), QString::number(userKey.modulus) });
}

static public_key_class FmtStrToPubKey(const QString& userFormattedKey) {
	QStringList components = fromFormatStr(userFormattedKey);
	public_key_class pubKey;
	pubKey.exponent = components[0].toLongLong();
	pubKey.modulus = components[1].toLongLong();
	return pubKey;
}

static private_key_class FmtStrToPrivKey(const QString& userFormattedKey) {
	QStringList components = fromFormatStr(userFormattedKey);
	private_key_class privKey;
	privKey.exponent = components[0].toLongLong();
	privKey.modulus = components[1].toLongLong();
	return privKey;
}

static void FmtStrToKeys(const QString& fmtStrKeys, public_key_class& pubKey, private_key_class& privKey) {

	QStringList components = fromFormatStr(fmtStrKeys);

	if (components.size() != 4) {
		fprintf(stdout, "Warning: Could not convert fmt string to keys\n");
		return;
	}

	pubKey.exponent = components.at(0).toLongLong();
	pubKey.modulus = components.at(1).toLongLong();
	privKey.exponent = components.at(2).toLongLong();
	privKey.modulus = components.at(3).toLongLong();
}

#endif // UTILS_H