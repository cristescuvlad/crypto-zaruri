#ifndef NETWORKDATA_H
#define NETWORKDATA_H

#include <QString>
#include <QList>
#include <QMap>

#include "rsa.h"


class NetworkData
{
public:
	void init(const QString& fileName);
	qlonglong getBalance(const public_key_class& userKey) const;
	void addData(const QString& key, const QString& data);
	QList<QString> getDataKeys();
	QList<QString> getDataValues();
	QString getValue(const QString& key);

private:
	void appendToFile(const QByteArray& data);
	void updateDataFile();

private:
	QMap<QString, QString> m_networkData; //(key, value) == (pub,priv ; balance,time)
	QString m_fileName; //TODO: secure?
};

#endif // NETWORKDATA_H