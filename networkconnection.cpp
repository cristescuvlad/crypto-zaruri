#include "networkconnection.h"

NetworkConnection::NetworkConnection(QObject *parent) : QObject(parent)
{
    m_buffer = new QByteArray();
}

NetworkConnection::~NetworkConnection() {
	disconnectFromHost();
	delete m_socket;
	m_buffer->clear();
    delete m_buffer;
}

bool NetworkConnection::connectToHost(QHostAddress address)
{
	if (m_socket) {
		delete m_socket;
        m_socket = nullptr;
	}
	m_socket = new QTcpSocket(this);

	m_socket->connectToHost(address, PORT);
	handleSocketEvents();

	return m_socket->waitForConnected(TIMEOUT); //search for a server in a blocking manner //TODO: move to thread?
}

void NetworkConnection::disconnectFromHost() {
	if (m_socket && m_socket->isOpen()) {
		m_socket->close();
	}
}

bool NetworkConnection::listenToHost(QHostAddress address, quint16 port)
{
	QTcpServer server;
	server.setMaxPendingConnections(1);

	if (server.listen(address, port)) {
        if (server.waitForNewConnection(3 * TIMEOUT)) { //wait for a connection in a blocking state //TODO: move to thread?

            m_socket = server.nextPendingConnection(); //get the socket to the connection
            m_socket->setParent(this); //remove link to server

            handleSocketEvents();

            server.close(); //then close the server
            return true;
        }
	}

    return false;
}

int NetworkConnection::sendData(const QByteArray* data) {
	//auto test = m_socket->localPort();
	//auto test2 = m_socket->peerPort();
    return m_socket->write(*data);
}

QByteArray* NetworkConnection::getReadData() const{
    return m_buffer;
}

void NetworkConnection::handleSocketEvents() {
	connect(m_socket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
	connect(m_socket, SIGNAL(disconnected()), this, SLOT(onSocketDisconnected()));
	connect(m_socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(onSocketDisconnected())); //TODO: onSocketError()
}

void NetworkConnection::onReadyRead() {
    m_buffer->append(m_socket->readAll());
}

void NetworkConnection::onSocketDisconnected() {
	emit(connectionStopped());
}