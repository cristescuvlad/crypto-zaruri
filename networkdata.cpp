#include "networkdata.h"
#include "utils.h"

#include <QFile>
#include <QTextStream>

//#include "Merkle/merkletree.h" //TODO hashing


void NetworkData::init(const QString& fileName)
{
	m_fileName = fileName;
	QFile networkDataFile(m_fileName);
	//TODO: move read to separate function
	if (!networkDataFile.open(QIODevice::ReadOnly)) {
		//TODO: 2 instances on the same computer using the same file ?
		fprintf(stdout, "Warning: Could not open network data file\n");
	}
	else {
		QTextStream in(&networkDataFile);

		while (!in.atEnd())
		{
			QStringList components = fromFormatStr(in.readLine());

			if (components.size() != 4) {
				//invalid network data file
				fprintf(stdout, "Warning: Network data file contains invalid data\n");
				continue;
			}

			QString nwUserKey = formatStrs({
				components[0],
				components[1]
			});

			QString userBalance = components.at(2);
			QString userCreationTime = components.at(3);

			//TODO: how to ensure user authenticity? how to choose latest data?
			addData(nwUserKey, formatStrs({ userBalance, userCreationTime }));
		}
		networkDataFile.close();
	}
	updateDataFile();
}

qlonglong NetworkData::getBalance(const public_key_class& userKey) const {

	qlonglong userBalance = 10; //TODO: think of a way for new users to earn coins

	const QString formattedKey = keyToFmtStr(userKey);

	if (m_networkData.contains(formattedKey)) {
		QStringList userValues = fromFormatStr(m_networkData.value(formattedKey));
		if (userValues.size() != 2) {
			//missing values
			return userBalance;
		}
		userBalance = userValues.at(0).toLongLong();
		//qlonglong userLastUpdate = userValues.at(1).toLongLong(); //unused for now
	}
	return userBalance;
}

void NetworkData::addData(const QString& key, const QString& data) {
	if (!m_networkData.contains(key)) {
		if (fromFormatStr(data).size() != 2) {
			//invalid data
			return;
		}
		m_networkData.insert(key, data);
	}
	else {
		//compare the old and new user creation time

		QString oldUserData = m_networkData.value(key);
		if (oldUserData == data) {
			//nothing to update
			return;
		}

		QStringList oldComponents = fromFormatStr(oldUserData);
		if (oldComponents.size() < 2) {
			//invalid data
			return;
		}
		qlonglong userOldUpdateTime = oldComponents.at(1).toLongLong();

		QStringList newComponents = fromFormatStr(data);
		if (newComponents.size() < 2) {
			//invalid data
			return;
		}
		qlonglong userNewUpdateTime = newComponents.at(1).toLongLong();

		//we are interested in the latest creation time to avoid duplicates //TODO: analyse this idea
		if (userNewUpdateTime > userOldUpdateTime) {
			m_networkData.insert(key, data);
		}
		else {
			//new update time is older
			return;
		}
	}

	QByteArray fileData = '\n' + formatStrs({ key, data }).toUtf8();
	appendToFile(fileData);
}

QList<QString> NetworkData::getDataKeys() {
	return m_networkData.keys();
}

QList<QString> NetworkData::getDataValues() {
	return m_networkData.values();
}

QString NetworkData::getValue(const QString& key) {
	return m_networkData.value(key);
}

void NetworkData::appendToFile(const QByteArray& data) {
	QFile networkDataFile(m_fileName);
	if (networkDataFile.open(QIODevice::Append)) {
		networkDataFile.write(data);
		networkDataFile.close();
	}
	else {
		//TODO: 2 instances on the same computer using the same file
		fprintf(stdout, "Warning: Could not append network data to file\n");
	}
}

void NetworkData::updateDataFile() {

	//TODO: what happens if we kill the process during this function?

	QFile networkDataFile(m_fileName);
	if (!networkDataFile.open(QIODevice::WriteOnly)) {
		//TODO: 2 instances on the same computer using the same file ?
		fprintf(stdout, "Warning: Could not open network data file\n");
	}
	else {
		if (!networkDataFile.resize(0)) {
			fprintf(stdout, "Warning: Failed to clear network data file before rewriting!\n");
		}

		QTextStream out(&networkDataFile);
		for (QString key : m_networkData.keys()) {
			out << formatStrs({key, m_networkData.value(key)}) + '\n';
		}
	}

	networkDataFile.close();

	//mt_t *merkleTree = mt_create();

	//QVector<QVector<uint8_t>> values; //TODO hashing
}