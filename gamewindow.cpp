#include "gamewindow.h"
#include "ui_gamewindow.h"

#include "qjsonobject.h"
#include "qjsonvalue.h"
#include "qjsondocument.h"
#include <QTimer>

GameWindow::GameWindow(NetworkConnection* peerConnection, bool isHost, const qlonglong& currentBalance, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GameWindow)
{
    ui->setupUi(this); 
	setAttribute(Qt::WA_DeleteOnClose);

	m_peerConnection = peerConnection; //so we pass the ownership to the gameWindow too
	m_isHost = isHost;
    m_timer = new QTimer(this);

	if (m_isHost) {
		ui->GameMessageLabel->setText("Your turn ...");
	}
	else {
		ui->GameMessageLabel->setText("Waiting for opponent's roll");
	}

	m_currentBalance = currentBalance;
	ui->CurrentBalanceLabel->setText("Current Balance: " + QString::number(m_currentBalance));

    connect(m_timer, SIGNAL(timeout()), this, SLOT(receiveGameDataFromNetwork()));
	connect(m_timer, SIGNAL(timeout()), this, SLOT(updateWindow()));
	connect(ui->RollADieButton, SIGNAL(clicked()), this, SLOT(onRollADieButtonClicked()));
	connect(m_peerConnection, SIGNAL(connectionStopped()), this, SLOT(onConnectionStopped()));

    m_timer->start();
}

GameWindow::~GameWindow()
{
	delete m_timer;
	delete ui;
	//delete m_peerConnection; //do not delete. mainwindow holds this pointer too. TODO: shared ptr?
}

qlonglong GameWindow::getLocalUserBalanceFromGame() {
	return m_currentBalance;
}

void GameWindow::sendGameDataToNetwork() {
	QJsonObject jsonObj;
	jsonObj.insert("bet", QJsonValue::fromVariant(m_betValue));
	jsonObj.insert("roll", QJsonValue::fromVariant(m_rollValue));

	QJsonDocument jsonDoc(jsonObj);
	QByteArray dataToSend = jsonDoc.toJson(QJsonDocument::Compact);
    int jsonSize = dataToSend.size();

    dataToSend.prepend((char*)&jsonSize, sizeof(int));

    if (m_peerConnection->sendData(&dataToSend) != dataToSend.size()) {
		fprintf(stdout, "Warning: Sent wrong amount of data.\n");
    }
}

bool GameWindow::isMyTurn() const{
	if (m_isHost) { //if I am host and opponent values were not received
		if (m_rollValue == -1 && m_betValue == -1 && m_opponentRollValue == -1 && m_opponentBetValue == -1) {
			return true;
		}
	}
	else { //if I am not the host and opponent values were received
		if (m_rollValue == -1 && m_betValue == -1 && m_opponentRollValue > 0 && m_opponentBetValue >= 0) {
			return true;
		}
	}
	return false;
}

void GameWindow::closeEvent(QCloseEvent *event) {
	QWidget::closeEvent(event);
	m_peerConnection->disconnectFromHost();
	emit(windowClosed());
}

void GameWindow::updateGameValues() { //TODO: update local network data

	//TODO: should the result be sent back?
	if (m_opponentRollValue > m_rollValue) {
		m_currentBalance -= m_betValue;
		ui->GameMessageLabel->setText("You lost :(");
	}
	else if (m_opponentRollValue < m_rollValue) {
		m_currentBalance += m_opponentBetValue;
		ui->GameMessageLabel->setText("You won !!!");
	}
	else {
		ui->GameMessageLabel->setText("It's a draw !!!");
	}

	ui->CurrentBalanceLabel->setText("Current Balance: " + QString::number(m_currentBalance));
	ui->OpponentBetValue_ValueLabel->setText(QString::number(m_opponentBetValue));
	ui->OpponentRollLCDNumber->display(QString::number(m_opponentRollValue));

	m_rollValue = -1;
	m_betValue = -1;
	m_opponentBetValue = -1;
	m_opponentRollValue = -1;
}

void GameWindow::onRollADieButtonClicked() {

	if (!isMyTurn()) {
		return;
	}
	
	m_betValue = ui->UserBetValueLineEdit->text().toUInt();

    if (m_betValue < 1) {
        m_betValue = -1;
        ui->GameMessageLabel->setText("Invalid bet!");
        return;
    }

    if (m_betValue > m_currentBalance) {
        m_betValue = -1;
        ui->GameMessageLabel->setText("You cannot bet more than you have!\n Your turn ...");
        return;
    }

    m_rollValue = qrand() % 6 + 1;

    ui->UserRollLCDNumber->display(QString::number(m_rollValue));

	sendGameDataToNetwork();
}

void GameWindow::onConnectionStopped() {
	emit(windowClosed());
}

void GameWindow::receiveGameDataFromNetwork() {

	QByteArray* receivedData = m_peerConnection->getReadData();

	if (receivedData->isEmpty()) {
		//TODO: Log failed to receive data
		return;
	}

	if (receivedData->size() < sizeof(int)) {
		return;
	}

	QDataStream stream(*receivedData);
	stream.setByteOrder(QDataStream::LittleEndian);

	int jsonSize = 0;
	stream.readRawData((char*)&jsonSize, sizeof(int));

	if (jsonSize == 0) {
		return;
	}

	if (receivedData->mid(sizeof(int)).size() < jsonSize) { //starting after the 4 bytes until the end
		return;
	}

	QJsonDocument receivedJsonDoc = QJsonDocument::fromJson(receivedData->mid(sizeof(int), jsonSize));
	QJsonObject receivedJsonObj = receivedJsonDoc.object();

	m_opponentBetValue = receivedJsonObj["bet"].toInt();
	m_opponentRollValue = receivedJsonObj["roll"].toInt();

	receivedData->remove(0, sizeof(int) + jsonSize);
}

void GameWindow::updateWindow() {

	if (m_opponentBetValue > 0 && m_opponentRollValue > 0) {
		if (m_isHost) {
			updateGameValues();
			ui->GameMessageLabel->setText(ui->GameMessageLabel->text() + "\nYour turn ...");
		}
		else {
			ui->OpponentBetValue_ValueLabel->setText(QString::number(m_opponentBetValue));
			ui->OpponentRollLCDNumber->display(QString::number(m_opponentRollValue));
			ui->GameMessageLabel->setText("Your turn ...");
		}
	}
	if (m_betValue > 0 && m_rollValue > 0) {
		if (!m_isHost) {
			updateGameValues();
			ui->GameMessageLabel->setText(ui->GameMessageLabel->text() + "\nWaiting for opponent's roll");
		}
		else {
			ui->GameMessageLabel->setText("Waiting for opponent's roll");
		}
	}
}